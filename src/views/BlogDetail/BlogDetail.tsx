import React from 'react'
import { Typography } from 'antd'
import { RightOutlined } from '@ant-design/icons'
import CardNews from 'components/Cards/CardNews'
import { newsData } from 'data/newsData'
import BreadCrumb from 'components/BreadCrumbs/BreadCrumbs'
import ParagrafDetailwork from 'components/paragraf-detailwork/ParagrafDetailwork'
import Share from 'components/Share/Share'

function CustomPagination({ total }) {
  return (
    <div className="flex space-x-2">
      {Array.from({ length: total }).map((_, index) => (
        <div
          key={index}
          className="w-3 h-3 mt-8 rounded-full bg-red-500 flex justify-center items-center cursor-pointer"
        >
          {index === total - 0 ? (
            <RightOutlined style={{ color: 'white' }} />
          ) : null}
        </div>
      ))}
    </div>
  )
}

function BlogDetail() {
  const breadcrumbItems = [
    { text: 'Blog', link: '/blog' },
    { text: '10 Ekstensi terbaik untuk Visual Studio Code' },
  ]
  return (
    <div className="container mx-auto">
      <div className="items-center justify-center m-[150px]">
        <BreadCrumb items={breadcrumbItems} />
        <Typography.Text type="secondary" className="text-xs mt-1">
          <span className="text-red-600 font-extrabold mr-1">Engineering</span>{' '}
          ❁<span className="text-gray-500 font-normal ml-2">18 Mei 2020</span>
        </Typography.Text>
        <br />
        <br />
        <h1 className="text-[42px] text-[#404258] font-semibold">
          10 Ekstensi terbaik untuk Visual Studio Code
        </h1>
        <Share />
        <img
          src="./static/images/coding-screen.jpeg"
          className="object-cover w-[78rem] h-full mt-8 border-rad"
          alt="konten"
          style={{ borderRadius: '30px' }}
        />
        <ParagrafDetailwork
          paragraf="Hallo temen-temen, pada kesempatan kali ini saya akan membahas tentang 10 Ekstensi terbaik untuk Visual Studio Code.  Text editor ini cocok bagi kalian yang stack di JavaScript & TypeScript, Kenapa ? Karena si Visual Studio Code itu sendiri  dibangun menggunakan TypeScript, jadi auto suggest / intelisense nya lebih mengarah ke TS / JS. Nah pada kesempatan kali ini  saya akan memberikan ekstensi terbaik untuk Visual Studio Code."
          styleParagraf="text-[#404258] text-[17px] w-full font-[400px] leading-[1.8]"
        />
        <h1 className="text-[40px] text-[#404258] font-semibold">Gitlab</h1>
        <ParagrafDetailwork
          paragraf="Hallo temen-temen, pada kesempatan kali ini saya akan membahas tentang 10 Ekstensi terbaik untuk Visual Studio Code. Text editor ini cocok bagi kalian yang stack di JavaScript & TypeScript, Kenapa ? Karena si Visual Studio Code itu sendiri dibangun menggunakan TypeScript, jadi auto suggest / intelisense nya lebih mengarah ke TS / JS. Nah pada kesempatan kali ini saya akan memberikan ekstensi terbaik untuk Visual Studio Code. 
          Hallo temen-temen, pada kesempatan kali ini saya akan membahas tentang 10 Ekstensi terbaik untuk Visual Studio Code.   Text editor ini cocok bagi kalian yang stack di JavaScript & TypeScript, Kenapa ? Karena si Visual Studio Code itu sendiri  dibangun menggunakan TypeScript, jadi auto suggest / intelisense nya lebih mengarah ke TS / JS. Nah pada kesempatan kali ini  saya akan memberikan ekstensi terbaik untuk Visual Studio Code."
          styleParagraf="text-[#404258] text-[17px] w-full font-[400px] leading-[1.8]"
        />
        <img
          src="./static/images/content/Blogdetail/gitlab.png"
          className="object-cover w-[78rem] mb-7 h-full"
          alt="konten"
        />
        <h1 className="text-[40px] text-[#404258] font-semibold">
          Visual Studio IntelliCode
        </h1>
        <ParagrafDetailwork
          paragraf="Sebenarnya auto suggest bawaan dari VS Code itu sendiri sudah lumayan bagus, saya menggunakan ini agar auto suggest bisa lebih detail suggestion yang diberikan, bisa diibaratkan Improve dalam hal suggestion ( Intellisense nya )."
          styleParagraf="text-[#404258] text-[17px] w-full font-[400px] leading-[1.8]"
        />
        <img
          src="./static/images/content/Blogdetail/intellicode.png"
          className="object-cover w-[78rem] mb-7 h-full"
          alt="konten"
        />
        <h1 className="text-[40px] text-[#404258] font-semibold">
          Sublime Text Keymap and Settings Importer
        </h1>
        <ParagrafDetailwork
          paragraf="Bagi teman-teman yang migrasi dari Sublime Text 3 ke Visual Studio Code dan merasa aneh dengan keymap di VS Code, bisa nih dicoba in keymap sublime text di VS Code."
          styleParagraf="text-[#404258] text-[17px] w-full font-[400px] leading-[1.8]"
        />
        <ParagrafDetailwork
          paragraf="Secara keseluruhan keymap nya sama persis dengan keymap sublime text."
          styleParagraf="text-[#404258] text-[17px] w-full font-[400px] leading-[1.8]"
        />
        <img
          src="./static/images/content/Blogdetail/sublime-keymap.png"
          className="object-cover w-[78rem] mb-7 h-full"
          alt="konten"
        />
        <h1 className="text-[40px] text-[#404258] font-semibold">File Utils</h1>
        <ParagrafDetailwork
          paragraf="Bagi teman-teman yang sering menggunakan VS Code tapi terbatas pada klik kanan di tampilan side folder project nya, yang hanya bisa copy dan paste. Nah pada ekstensi ini ada beberapa penambahan seperti Duplicate, Move, dan masih banyak lagi."
          styleParagraf="text-[#404258] text-[17px] w-full font-[400px] leading-[1.8]"
        />
        <img
          src="./static/images/content/Blogdetail/utils.png"
          className="object-cover w-[78rem] mb-7 h-full"
          alt="konten"
        />
        <h1 className="text-[40px] text-[#404258] font-semibold">Prettier</h1>
        <ParagrafDetailwork
          paragraf="Ekstensi ini cocok bagi teman-teman yang berfokus pada JavaScript, selain dia merapikan kodingan, kalo kita integrasi kan dengan ESLint dia bisa mebenerin kodingan yang sesuai standar, misalkan kita bikin var formatNumber, nah karna var ini terlalu global, nanti nya ESLint dan Prettier akan mengubah ke const formatNumber kalo variabel itu cuma di read / tidak ada perubahan nilai pada variabel."
          styleParagraf="text-[#404258] text-[17px] w-full font-[400px] leading-[1.8]"
        />
        <img
          src="./static/images/content/Blogdetail/prettier.png"
          className="object-cover w-[78rem] mb-7 h-full"
          alt="konten"
        />
        <Share />
        <div className="flex justify-center mt-6 ">
          <CustomPagination total={3} />
        </div>
      </div>

      {/* Related */}
      <div className="flex justify-between items-center">
        <div className="flex gap-3">
          <h1 className="text-[42px] text-[#404258] font-semibold">
            You might be like
          </h1>
        </div>
      </div>
      <div className="flex gap-[24px] justify-center mt-7">
        {newsData.map((e) => (
          <CardNews
            size="w-[411px]"
            image={e.img}
            title={e.title}
            category={e.category}
            date={e.date}
          />
        ))}
      </div>
      {/* end */}
    </div>
  )
}

export default BlogDetail
